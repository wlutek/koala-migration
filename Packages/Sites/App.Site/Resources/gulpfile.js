var gulp = require('gulp'),
    php = require('gulp-connect-php'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    cache = require('gulp-cache'),
    concat = require('gulp-concat'),
    extname = require('gulp-extname'),
    imagemin = require('gulp-imagemin'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    assemble = require('assemble'),
    babelify = require('babelify'),
    browserify = require('browserify'),
    buffer = require('vinyl-buffer'),
    source = require('vinyl-source-stream'),
    del = require('del'),
    gutil = require('gulp-util'),
    vendor = require('gulp-concat-vendor'),
    inject = require('gulp-inject');
    uglify = require('gulp-uglify');
    cleanCSS = require('gulp-clean-css');


var assembleApp = assemble();

var dirs = {
    src: 'Src',
    dest: 'Public',
    private: 'Private'
};

var paths = {
    src: {
        css: dirs.src + '/Styles/main.scss',
        cssWatch: dirs.src + '/Styles/**/*',
        html: {
            pages: dirs + '/Templates/**/*.html'
        },
        images: dirs.src +'/Images/**/*',
        js: dirs.src +'/Scripts/**/*.js',
        jsVendors: dirs.src +'/Scripts/vendors/**/*.js',
        fonts: dirs.src + '/Fonts/*'
    },
    dest: {
        baseDir: dirs.dest,
        css: dirs.dest +'/Styles',
        html: dirs.private +'/',
        images: dirs.dest +'/Images',
        js: dirs.dest +'/JavaScript',
        fonts: dirs.dest + '/Fonts'
    }
};

// ------ scripts concat and uglify -----
gulp.task('concat-min', function() {
    return gulp.src(['./Public/JavaScript/vendors.js', './Public/JavaScript/app.js'])
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dest.js))
});

// ------ css clean and minify -----
gulp.task('minify-css', function() {
    return gulp.src('./Public/Styles/main.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(paths.dest.css));
});

// ----- clean -----
gulp.task('clean:dist', function(){
    return del(dirs.dest + '/**/*.*', {force: true})
});
gulp.task('clean:html', function () {
    return del(paths.dest.html, {force: true})
});
gulp.task('clean:images', function () {
    return del(paths.dest.images, {force: true})
});
gulp.task('clean:fonts', function () {
    return del(paths.dest.fonts, {force: true})
});

// ----- load -----
gulp.task('load', function (cb) {
    assembleApp.partials(paths.src.html.partials);
    assembleApp.layouts(paths.src.html.layouts);
    assembleApp.pages(paths.src.html.pages);
    cb();
});

// ----- browser-sync -----
gulp.task('browser-sync', function() {
    browserSync({
        proxy: 'metalkas-blog.dev',
        open: true,
        notify: false
    });
});
// ----- bs-reload -----
gulp.task('bs-reload', function () {
    browserSync.reload();
});

// ----- images -----
gulp.task('images', function () {
    gulp.src(paths.src.images)
        .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
        .pipe(gulp.dest(paths.dest.images));
});

// ----- styles -----
gulp.task('styles', function (){
    gulp.src([paths.src.css])
        .pipe(buffer())
        .pipe(sourcemaps.init())
        .pipe(sass())
         .on('error', function (err) {
             var displayErr = gutil.colors.red(err);
             gutil.log(displayErr);
             gutil.beep();
         })
        .pipe(autoprefixer('last 2 versions'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.dest.css))
});

// ----- scripts -----
gulp.task('scripts', function (){
    var bundler = browserify({
        entries: 'Src/Scripts/app.js',
        debug: true
    });
    bundler.transform(babelify);

    bundler.bundle()
        .on('error', function (err) { console.error(err); })
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.dest.js))
        .pipe(browserSync.reload({stream:true}));
});

// ------ script vendors -----
gulp.task('scriptsVendors', function(){
    return gulp.src([
        'Src/Scripts/vendors/test.js',
        'Src/Scripts/vendors/jQuery-2.2.0.min.js',
        'Src/Scripts/vendors/angular.js',
        'Src/Scripts/vendors/angular.localstorage.js',
        'Src/Scripts/vendors/bootstrap.min.js',
        'Src/Scripts/vendors/enquire.js',
        'Src/Scripts/vendors/jquery.matchHeight.js',
        'Src/Scripts/vendors/jquery.waypoint.js',
        'Src/Scripts/vendors/slick.js',
        'Src/Scripts/vendors/slick-lightbox.js',
        'Src/Scripts/vendors/countUp.js',
        'Src/Scripts/vendors/parsley.min.js',
        'Src/Scripts/vendors/classie.js',
        'Src/Scripts/vendors/ngPaginator.js',
        'Src/Scripts/vendors/moment.js',
        'Src/Scripts/vendors/magnificpopup.js',
        'Src/Scripts/vendors/angularVertilize.js',
        'Src/Scripts/vendors/lozalize_pl.js',
        'Src/Scripts/vendors/angular-timer.js',
        'Src/Scripts/vendors/ui-bootstrap-custom-2.5.0.js',
        'Src/Scripts/vendors/ui-bootstrap-custom-tpls-2.5.0.js',
        'Src/Scripts/vendors/select2.js',
        'Src/Scripts/vendors/sanitize.js'

        ])
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest(paths.dest.js))

});

// ----- fonts -----
gulp.task('fonts', function() {
    return gulp.src([paths.src.fonts])
        .pipe(gulp.dest(paths.dest.fonts));
});

// ----- default -----
gulp.task('default', ['browser-sync'], function (){
    gulp.watch(paths.src.cssWatch, ['styles', 'minify-css']);
    gulp.watch(paths.src.js, ['scripts', 'concat-min']);
    gulp.watch(paths.src.images, ['clean:images', 'images']);
    gulp.watch(paths.src.html.templates, ['assemble', 'indexhtml']);

    // ----- manual pathing -----
    // watching YAML files in Configuration folder
    gulp.watch("../Configuration/**/*.yaml", ['bs-reload']);
    // watching HTML files in Public/Templates
    gulp.watch(paths.dest.html + "/*.html", ['bs-reload']);
    // watching HTML files in Public/Templates
    gulp.watch(paths.dest + "TypoScript/**/*.ts2", ['bs-reload']);
    gulp.watch(paths.dest.css + "/*.css", ['bs-reload']);
    gulp.watch(paths.src.fonts, ['clean:fonts', 'fonts']);
});

// ----- indexhtml -----
gulp.task('indexhtml', function() {    
    gulp.src(paths.src.html.index)
    .pipe(inject(
        gulp.src([paths.dest.html + '/*.html'], {read: false}), {
        transform: function (filepath) {
            filepath = filepath.replace(paths.dest.html + '/', '');
            
            if (filepath.slice(-5) === '.html' && !(filepath.slice(-10) === 'index.html')) {
                return '<li><a href="' + filepath + '">' + filepath + '</a></li>';
            }
        }
    }
    ))
    .pipe(gulp.dest(paths.dest.html)); 
});

// ----- build -----
gulp.task('build', ['clean:dist', 'styles', 'scripts', 'scriptsVendors', 'concat-min', 'minify-css']);