var APP, lastScrollTop;

APP = {};

APP.animateCss = function () {
    return $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd;
            animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            return this.addClass('animated ' + animationName).one(animationEnd, function () {
                return $(this).removeClass('animated ' + animationName);
            });
        }
    });
};

lastScrollTop = false;

APP.navbarScrolling = function () {
    $(document).scroll(function (event) {
        var st;
        st = $(this).scrollTop();
        if (st > lastScrollTop) {
            $('.head-not-scrolling').removeClass('hidden');
            $('.head-scrolling-up').removeClass('navbar--scrolling').addClass('hidden');
        } else {
            $('.head-not-scrolling').addClass('no-visibility').removeClass('animated fadeIn');
            $('.head-scrolling-up').addClass('animated slideInDown').removeClass('hidden');
        }
        if (st < 1) {
            $('.head-not-scrolling').removeClass('no-visibility').addClass('animated fadeIn');
            $('.head-scrolling-up').addClass('hidden');
        }
        if (st === 0) {
            $('.head-not-scrolling').removeClass('animated fadeIn');
        }
        lastScrollTop = st;
    });
};

APP.slick = function () {
    $('.partners-slick').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
        }]
    });
    $('.realisations-slick').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
        }]
    });
    $('.references-slick').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
    });
    $('.related-slick').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
        }]
    });
};

APP.desktopMenu = function () {
    var TIMEOUT, elBtn, elRoot, hideContainer, showContainer;
    elRoot = $('#desktop-menu');
    elBtn = elRoot.find('.menu-btn');
    TIMEOUT = 200;
    hideContainer = function (elButton, elContainer) {
        var delay;
        delay = function () {
            if (elContainer.filter(':hover')) {
                elContainer.on('mouseleave', function () {
                    elButton.removeClass('inAction');
                    elContainer.removeClass('show');
                    elContainer.off('mouseleave');
                });
            } else {
                elButton.removeClass('inAction');
                elContainer.removeClass('show');
            }
        };
        setTimeout(delay, TIMEOUT);
    };
    showContainer = function (elButton, elContainer) {
        var delay;
        delay = function () {
            hideContainer(elButton, elContainer);
            elButton.off('mouseleave');
        };
        elButton.addClass('inAction');
        elContainer.addClass('show');
        elButton.on('mouseleave', function () {
            setTimeout(delay, TIMEOUT);
            elButton.removeClass('inAction');
        });
    };
    elBtn.on('mouseenter', function () {
        showContainer($(this), $($(this).data('display-container')));
    });
};

APP.desktopMenuScrolling = function () {
    var TIMEOUT, elBtn, elRoot, hideContainer, showContainer;
    elRoot = $('#desktop-menu-scrolling');
    elBtn = elRoot.find('.menu-btn');
    TIMEOUT = 200;
    hideContainer = function (elButton, elContainer) {
        var delay;
        delay = function () {
            if (elContainer.filter(':hover')) {
                elContainer.on('mouseleave', function () {
                    elButton.removeClass('inAction');
                    elContainer.removeClass('show');
                    elContainer.off('mouseleave');
                });
            } else {
                elButton.removeClass('inAction');
                elContainer.removeClass('show');
            }
        };
        setTimeout(delay, TIMEOUT);
    };
    showContainer = function (elButton, elContainer) {
        var delay;
        delay = function () {
            hideContainer(elButton, elContainer);
            return elButton.off('mouseleave');
        };
        elButton.addClass('inAction');
        elContainer.addClass('show');
        elButton.on('mouseleave', function () {
            setTimeout(delay, TIMEOUT);
            elButton.removeClass('inAction');
        });
    };
    elBtn.on('mouseenter', function () {
        showContainer($(this), $($(this).data('display-container')));
    });
};

APP.search = function () {
    $('.head__search--button').click(function () {
        $(this).addClass('head__search--button--hidden');
        $('.head__search--buttonSubmit').addClass('head__search--buttonSubmit--show');
        $('.head__search--input').addClass('head__search--input--open');
    });
    $('.head').mouseleave(function () {
        $('.head__search--input').val('');
        $('.head__search--button').removeClass('head__search--button--hidden').show();
        $('.head__search--buttonSubmit').removeClass('head__search--buttonSubmit--show');
        $('.head__search--input').removeClass('head__search--input--open');
    });
};

APP.init = function () {
    APP.animateCss();
    APP.navbarScrolling();
    APP.slick();
    APP.desktopMenu();
    APP.desktopMenuScrolling();
    APP.search();
};

$(document).ready(function () {
    APP.init();
});
