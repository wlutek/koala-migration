$ ->
  new WOW().init();
  mySwiper = new Swiper('.swiper-container', {
    speed: 600,
    spaceBetween: 0,
    autoHeight: true,
    autoplay: 8000,
    watchSlidesProgress: true,
    parallax: true,
    loop: true,
    keyboardControl: true,
    paginationClickable: true,
    nextButton: '.top-nav-next',
    prevButton: '.top-nav-prev'
  })
  mySwiper2 = new Swiper('.swiper-products', {
    speed: 600,
    spaceBetween: 10,
    autoHeight: true,
    autoplay: 3000,
    parallax: false,
    loop: true,
    paginationClickable: true,
    nextButton: '.prod-nav-next',
    prevButton: '.prod-nav-prev',
    slidesPerView: 3,
    breakpoints: {
      1024: {
        slidesPerView: 2,
        spaceBetween: 20,
        parallax: false
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      480: {
        slidesPerView: 1,
        spaceBetween: 10,
        parallax: true
      }
    }
  })
  mySwiper3 = new Swiper('.swiper-paralax', {
    speed: 600,
    spaceBetween: 10,
    autoHeight: true,
    autoplay: 3000,
    parallax: false,
    loop: true,
    paginationClickable: true,
    nextButton: '.para-nav-next',
    prevButton: '.para-nav-prev',
    slidesPerView: 2,
    breakpoints: {
      1024: {
        slidesPerView: 2,
        spaceBetween: 20,
        parallax: false
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      480: {
        slidesPerView: 1,
        spaceBetween: 10,
        parallax: true
      }
    }
  })
  #  #  $('.slider').slick({arrows: false, autoplay: true});
  #  $('.slider-strong').slick({
  #    arrows: false, centerMode: true,
  #    autoplay: true,
  #    responsive: [
  #      {
  #        breakpoint: 980,
  #        settings: {
  #          slidesToShow: 2,
  #          arrows: false
  #        }
  #      }, {
  #        breakpoint: 768,
  #        settings: {
  #          slidesToShow: 2
  #        }
  #      },
  #      {
  #        breakpoint: 480,
  #        settings: {
  #          slidesToShow: 1,
  #          centerMode: true
  #        }
  #      }]
  #  });

  $('#toTop').click(()->
    $("html, body").animate({scrollTop: 0}, 600)
  )
  $(window).on("scroll", ()->
    scrollPos = $(window).scrollTop()
    if (scrollPos <= 0)
      $("#toTop").fadeOut()
    else
      $("#toTop").fadeIn()
  )

