<?php
namespace DevHero\Newsletter\Domain\Repository;

/*
 * This file is part of the DevHero.Newsletter package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class NewsletterRepository extends Repository
{
    // add customized methods here

}
