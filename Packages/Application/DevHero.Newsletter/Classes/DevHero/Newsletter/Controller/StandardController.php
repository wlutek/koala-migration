<?php
namespace DevHero\Newsletter\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "DevHero.Mailer".        *
 *                                                                        *
 *                                                                        */

use DevHero\Newsletter\Domain\Model\Newsletter;
use DevHero\Newsletter\Domain\Repository\NewsletterRepository;
use Neos\Flow\Mvc\Controller\ActionController;
use Neos\Flow\Persistence\Doctrine\PersistenceManager;
use Swift_Message;
use Neos\Flow\Annotations as Flow;

class StandardController extends ActionController
{

    protected $supportedMediaTypes = array('application/json');

    /**
     * @var array
     *
     */
    protected $viewFormatToObjectNameMap = array('json' => 'Neos\Flow\Mvc\View\JsonView');

    static $target = array();

    /**
     * @Flow\Inject
     * @var NewsletterRepository
     */
    protected $newsletter;

    /**
     * @Flow\Inject
     * @var PersistenceManager
     */
    protected $pm;

    /**
     * @return void
     */
    public function subscribeAction()
    {

        $referer = $_SERVER['HTTP_REFERER'];
        $data = json_decode($this->request->getHttpRequest()->getContent(), true);
        $ob = new Newsletter();
        $ob->setEmail($data['mail']);
        $ob->setEnabled(true);
        $this->newsletter->add($ob);
        $this->pm->persistAll();
        $this->view->assign('value', $data);
    }

}