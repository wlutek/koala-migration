<?php
namespace DevHero\Contact\ViewHelpers;

use Neos\FluidAdaptor\Core\ViewHelper\AbstractViewHelper;

class ParseViewHelper extends AbstractViewHelper
{

    /**
     * @var boolean
     */
    protected $escapeChildren = false;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @param mixed $value The value to output
     * @return string
     */
    public function render($value = null)
    {
        if ($value === null) {
            $value = $this->renderChildren();
        }
        $message = '<ul>';
        $data = json_decode($value, true);
        foreach ($data as $value) {
            $message .= '<li><a href="' . $value['link'] . '" target="_blank">' . $value['label'] . '</a>';
        }
        $message .= '</ul>';
        return $message;
    }
}