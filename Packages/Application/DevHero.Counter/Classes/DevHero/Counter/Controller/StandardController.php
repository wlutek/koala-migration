<?php

namespace DevHero\Counter\Controller;

/*                                                                        *
 * TThis file is part of the DevHero.Counter package.        *
 *                                                                        *
 *                                                                        */

use Neos\ContentRepository\Domain\Model\NodeData;
use Neos\ContentRepository\Domain\Repository\NodeDataRepository;
use Neos\ContentRepository\Domain\Repository\WorkspaceRepository;
use Neos\Flow\Mvc\Controller\ActionController;
use Neos\Flow\Persistence\Doctrine\PersistenceManager;
use Neos\Flow\Annotations as Flow;

class StandardController extends ActionController
{


    /**
     * @Flow\Inject
     * @var NodeDataRepository
     */
    protected $nodeData;

    /**
     * @Flow\Inject
     * @var WorkspaceRepository
     */
    protected $workspaceRepository;
    /**
     * @Flow\Inject
     * @var PersistenceManager
     */
    protected $pm;

    public function indexAction()
    {
        /** @var NodeData $node */
        $node = $this->request->getInternalArgument('__parentNode');
        $node->setProperty('counter', $node->getProperty('counter')+1);
        $this->pm->persistAll();

    }

}